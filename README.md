# Tech & Code Crackme I


## Goal

```
The goal is to make the program say ‘‘valid license’’.   You
have two options in how to achieve this goal:

1.   Reverse engineer the validation algorithm.  Write a key
     generator.  Generate a valid key.  Input that key  into
     the executable.

2.   Successfully  exploit  the program so that you can make
     it output either the string in the  executable  or  any
     arbitrary string and make it read ‘‘valid license’’.

     Option 2 may only exist in theory.  The program has not
been designed to be exploited.  No exploitation is necessary
to solve this crackme.

     Modifying  the  executable  is not permitted.  The exe‐
cutable must exit with exit status 0.
```


## Hackerman :registered:

```sh
#!/bin/sh
wget https://scotteh.me/xhcrackme.tar.gz
tar xf xhcrackme.tar.gz
cd xhcrackme
gcc -shared -ox -xc - <<X
#include<stdio.h>
#include<stdlib.h>
size_t strlen(const char *s) { printf("%s\nvalid license\n", s); exit(0); }
X
LD_PRELOAD=./x ./license -c 'I can shitpost'
```

While technically correct and fulfilling option 2, it's almost like...
cheating ? :feelsgood:


## Setup

First of all, that's some sketchy unknown proprietary binary blob shit posting
OS, so I've setup a sandbox VM to do the reversing. I picked a headless qemu
with debian 9 that I access with ssh.

```sh
qemu-system-x86_64 -hda debian.img -m 4G -smp 8 -nographic -enable-kvm -net user,hostfwd=tcp::10022-:22 -net nic
```

Then get [shitpostOS](xhcrackme.tar.gz) :tm: which conveniently includes a
manpage but why read it when you can just ask on #techcode support channel.

## Poke it

There are many ways to first get an idea of an executable's behavior, tools
like `ltrace`, `strace`, `objdump` and even `hexdump` are good starters.

The challenge specifies that the executable must print "valid license".

```sh
$ grep -abo 'valid license' license
107199:valid license
107405:valid license
107491:valid license

$ bc <<< 'obase=16;107491'
1A3E3
```

That's the address where the string is stored, the two others are actually
"[...]invalid license".


## Analysis

I used [radare2](https://github.com/radare/radare2) to do the reversing job,
it's really neat. I only scratched the surface so far and it has great
potential.

### Static analysis

So let's do some scouting with it :

```
r2 -A license          # start radare with analysis
[0x00001df0]> 0x1a3e3  # let's go check that valid license
[0x0001a3e3]> V        # go into visual mode

[0x0001a3e3 67% 816 license]> x @ str.valid_license
- offset -   0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x0001a3e3  7661 6c69 6420 6c69 6365 6e73 6500 636f  valid license.co
...
```

Check where that address is referenced by typing `x<enter>` and
switch mode to assembly with `p`.

That leads to `0x18ce3`, which loads the address to the string as
an argument for `puts` libc function.

Let's see if that's inside a function, type `N` to seek the previous
function definition.

It sets the cursor at `0x18c70`, and radare nicely commented the callers.

```
[0x00018c70 63% 255 license]> pd $r @ sub.puts_c70
/ (fcn) sub.puts_c70 275
|   sub.puts_c70 ();
|           ; CALL XREF from 0x000188fb (unk)
|           ; CALL XREF from 0x00018a1b (unk)
|           ; CALL XREF from 0x00018b35 (unk)
```

So this is a first interest point here, the program needs to get into this
function to be valid.

> Mark the spot with `mX` where X is any key to go back quickly with `'X`

Turns out that if I take any caller by pressing `xN` where N is the one to
select, and then press `N` like earlier, they all lead to `0x188a0` :

```
[0x000188a0 62% 279 license]> pd $r @ fcn.000188a0
/ (fcn) fcn.000188a0 2
|   fcn.000188a0 ();
|           ; CALL XREF from 0x000100c3 (unk)
\           0x000188a0      f3c3           ret
            0x000188a2      66666666662e.  nop word cs:[rax + rax]
			; DATA XREF from 0x00001e0d (entry0)
            0x000188b0      55             push rbp
```
Hey there `entry0`, that looks like `main` after the whole libc init.

Before diving into the gory debugging, there's one more observation to make :
```
0x000188f3      e8e8020000     call sub.fopen_be0 ; file*fopen(const char *filename,
0x000188f8      4889ef         mov rdi, rbp
0x000188fb      e870030000     call sub.puts_c70  ; int puts(const char *s);
```

`sub.puts_c70` is our address at `0x18c70`, it seems radare gives it a name
depending on calls it finds inside the function.
So what about `sub.fopen_be0`, let's go check it.
Move down with the arrows or hjkl to the call and then `<enter>`.

It looks like a short function, and with radare comments, we can easily see that it
opens the file "/etc/license" and reads it's content.  So `0x18be0` is also an
address to take note of.

#### Wrap-up

3 points of interest :

```
0x000188b0 ; main entry point

0x00018be0 ; open/read/close "/etc/license"

0x00018c70 ; prints (in)valid license
```

### Runtime investigation

It's time for runtime analysis and try to understand what does the program
expect.

It's expecting to read `/etc/license`, so let's just create the file.

```sh
$ su
$ echo xyz > /etc/license
$ chmod `id -u devil` /etc/license
```

Back to radare, let's fire up a debugging session with the information
garthered.

Open the command dialog with `:` :
```
doo                         # reload radare in debugging mode
db section.LOAD0+0x188b0    # break at main
db section.LOAD0+0x18be0    # break at fopen_be0
db section.LOAD0+0x18c70    # break at puts_c70
dc                          # continue
aaa                         # run analysis
```

> Commands can be separated with `;` on a single line, which makes it easier
> to recall them

`section.LOAD0` is a symbol that let's us reference the memory address where
the `.text` segment of the program is loaded, since it will change everytime
the program restarts.

Let's press `p` again to show the registers state in visual mode.

Now at the entry point, let's continue to the `sub.fopen_be0`, press `<F9>`.

Then let's skip to the most intersting part, `fread`, step with `S` (step over)
right after `fread`.

The signature of fread is
`size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream);`

The arguments are :
1. Pointer to the memory where the content will be put
2. Size of an item
3. Number of items
4. File pointer

By convention the first six arguments are stored in order into
`rdi rsi rdx rcx r8d r9d`

Type `:ps @rdi` and it will display the content `xyz` that was read.
Also note that `rsi` was set to `0x1d` and `rdx` to 1, which means that
it reads 29 bytes at most. It also makes sense with the NUL terminator
that is set a little further :
```
mov byte [r14 + 0x1d], 0
```

With that information let's try again by setting the license to
something that reflects this behavior :
```
abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
```

And restart radare : `:doo; db section.LOAD0+0x18c17; dc`

Now the content pointed by `rdi` shows `abcdefghijklmnopqrstuvwxyzABC`

Let's just return from the function by stepping, until we hit `ret`.
Back into our main function, the returned pointer contained in `rdp`
is passed to the next function, the one that prints "valid license".

Into `sub.puts_c70`, let's continue stepping until...
we get branched away from the valid license at `0x18cc9` !


### Backtrack

```
mov eax, dword [r15 - 0x18]
lea ecx, dword [rax + 2]
cmp ecx, 2
jb  ...
```

This test gives the wrong branch, let's first understand what it expects.

Register `rcx` contains `0x00000000` and that's below 2, so it branches away;

- `rcx` is computed from `rax + 2`
- `rax` value comes from the memory at `r15 - 0x18`
- `r15` is loaded from `fs:[r14]`
- `r14` is `-0x28`

Finding where the value pointed by `r15 - 0x18` is set was a lot searching but
to summarize, there are couple ways to find it :

- The content of `rax` was `0xfffffffe`, look for that value
- Find all the places where `fs:[-0x28]` is accessed directly and/or indirectly
- Actually just step in the function `sub.strlen_d90` @ `0x18d90` that's
  in `sub.puts_c70` to find out that it happens right there !
  (I didn't do that at first..)


#### First step

Now skimming through to that function at `0x18d90`, there's the actual `strlen`
function that's called at `0x18de7`. It also happens that the parameter is the
string from the license file. Right after, it checks that it's length is `0x1d`.

The license **must** be 29 characters long, anything longer is
just truncated and won't impact the checking process,
anything shorter fails that test.


#### Second step

There's a repeating pattern after that :
```
mov al, byte [r13 + N]
mov ecx, eax
add cl, 0xd3
cmp cl, 1
ja  ...
```
For every N in [5,0xb,0x11,0x17]

Basically, it checks that the character (*c*) at that position in the license
respects : `( c + 0xd3 ) % 0x100 <= 1`

There's only 2 characters that respect this equation `0x2d` and `0x2e`
which are literally `-` and `.`.

After the character check it also ensures that the same character was used
at each position by comparing 3 different pairs:
```
cmp dl, cl

cmp al, dl

cmp r10b, al
```
Each of these register contains a separator character value at different
positions.

Let's update the license to pass this :
```
abcde-fghij-klmno-pqrst-uvwxy
```

### Third step

Stepping a little further after some setup (let's just skip it for now),
there's a loop from `0x18e90` to `0x18ee0` that iterates on the license
string.

It loads the character value into `rdx`, and this time it checks :
`( c + 0xd3 ) % 0x100 >= 2`

Meaning any character **but** the separators `-` and `.`.

Then verifies that :
`c - 0x32 <= 0x48` <=> `0x32 <= c <= 0x7a`

`0x7a` is the character `z` and *c* cannot be lower than `0x32` or `2`,
otherwise it will underflow. So that sets the minimum to 0x32.

So the range of valid bytes is `[ 0x32 - 0x7a ]` so far.

Next check is done against a static area of memory starting at
`0x1e300 - 0xc8`.  `r9` contains the pointer to that region from
the skipped setup. And it validates that the double word at offset
4*c* dosen't flag negative. Here's a snaphot of that memory region with
double word integer representation :

```
[0x0001e238]> pxd4 (0x7a-0x32)*4 @0x1e300-0xc8+(4*0x32)
- offset -    0  1   2  3   4  5   6  7   8  9   A  B   C  D   E  F  0123456789ABCDEF
0x0001e300             20            21            22            23  ................
0x0001e310             24            25            -1            -1  ................
0x0001e320             -1            -1            -1            -1  ................
0x0001e330             -1            -1            -1            -1  ................
0x0001e340              0             1             2            -1  ................
0x0001e350              3             4             5            -1  ................
0x0001e360             -1             6             7             8  ................
0x0001e370              9            -1            10            11  ................
0x0001e380             12            13            14            -1  ................
0x0001e390             15            16            17            18  ................
0x0001e3a0             19            -1            -1            -1  ................
0x0001e3b0             -1            -1            -1            -1  ................
0x0001e3c0              0             1             2            -1  ................
0x0001e3d0              3             4             5            -1  ................
0x0001e3e0             -1             6             7             8  ................
0x0001e3f0              9            -1            10            11  ................
0x0001e400             12            13            14            -1  ................
0x0001e410             15            16            17            18  ................
```

If it maps to a negative number, it fails the test.  Listing individual values
the list contains the following addresses that must be
blacklisted *{consecutive double words}* :
```
0x1e318 {10}
0x1e34c
0x1e35c {2}
0x1e374
0x1e38c
0x1e3a4 {7}
0x1e3cc
0x1e3dc {2}
0x1e3f4
0x1e40c
```

Transform every value to the corresponding byte :
```
0x38 8
0x39 9
0x3a :
0x3b ;
0x3c <
0x3d =
0x3e >
0x3f ?
0x40 @
0x41 A
0x45 E
0x49 I
0x4a J
0x4f O
0x55 U
0x5b [
0x5c \
0x5d ]
0x5e ^
0x5f _
0x60 `
0x61 a
0x65 e
0x69 i
0x6a j
0x6f o
0x75 u
```

So filtering everything we end up with the following possible characters :
```
234567
BCDFGHKLMNPQRSTVWXYZ
bcdfghklmnpqrstvwxyz
```

Analyzing the loop a little more, the value used to blacklist characters is
also used for a kind of checksum stored in `rax`. Using the memory
snapshot we can come up with the following mapping :

```
B C D F G H K L M N P Q R S T V  W  X  Y  Z
b c d f g h k l m n p q r s t v  w  x  y  z  2  3  4  5  6  7 : character
- - - - - - - - - - - - - - - - -- -- -- -- -- -- -- -- -- --
0 1 2 3 4 5 6 7 8 9 a b c d e f 10 11 12 13 14 15 16 17 18 19 : hex value
```

This correlates with the fact that when you give a lower case license, it
prints it back in upper case, so the license could be case incensitive.

### Post validation

So far we have pretty much passed all the premliminary checks, we can proceed
to `0x18ee2`. Although now there's a couple bitshift operations that use some
registers that were updated during the character loop check...

Before jumping into deeper analysis let's just go further and see what's
coming.

At `0x18f6a` we see that a check is made on the first separator character
testing whether it's a `.`. Looking at both branch paths, they end up back on
the same track. So that indicates that the separator type has a certain impact.
Again, let's just take it into consideration and continue.

Starting around `0x18fdf` is the juicy part, all the crypto stuff is called
there. Beside that, it's pretty straight forward, if run with a license
that pass the preliminairy checks, it'll go through up to `0x1914f`.
So beside some error checking, it seems that this part is fairly branchless.


## Interlude

There's enough information so far to try something.

Recap :

- Fixed length of 29
- Same separators (`.`/`-`) at fixed intervals
- Definite set of characters
- Separator type has an influence

Here's a quick script :

```sh
#!/bin/bash

chunk () {
    tr -dc '234567BCDFGHKLMNPQRSTVWXYZbcdfghklmnpqrstvwxyz' < /dev/urandom | head -c5
}

generate () {
    echo -n `chunk`.`chunk`.`chunk`.`chunk`.`chunk`
}

for i in {1..6}
do {
    while :
    do
        dot=`generate`
        dash=`echo $dot | tr . -`
        ./license -c $dot
        ./license -c $dash
    done
} 2>/dev/null >> out &
```

It will produce a bunch of licenses, and it's already giving interesting output
like "license for wrong version of shitpostOS"
and "evaluation license, expires: Mon Jan  3 22:35:35 2056".
So it has potential, let's leave it run in the background.

## Last stretch

Back to the reversing while the comupter is doing some backgound work.
Let's recall where the last check failed before the valid license at `0x18cc9`.
So there's this value in `rax` and if we add 2 it should not be below 2.
And right after, it checks that `rax` must be 1. `rax` is loaded from
`r15 - 0x18` and `r15` comes from `fs:[-0x28]`.

From debugging experience, it's possible to tell that `r15 - 0x18` value is
`0xfffffffe` when invalid. This value is set at `0x18ddd` inside the function
that checks the license. So in that function, `r15` contains the pointer to the
value that we need set to 1. That should probably happen near the end of the
function after all the tests passed. Let's highlight `r15` by typing `:/r15`
and make our way down the function to make sure `r15` stays intact. On the way
we see that the value pointed by `r15` is changed at `0x18ee2`, that's right
after the character loop check. Continue downward until `0x191c1`, that's
where the value that we need is set to 1.

Let's analyze the sequence :
```
0x0001919d      0fb64abc       movzx ecx, byte [rdx - 0x44]
0x000191a1      0fb642bd       movzx eax, byte [rdx - 0x43]
0x000191a5      c1e008         shl eax, 8
0x000191a8      09c8           or eax, ecx
0x000191aa      4889d1         mov rcx, rdx
0x000191ad      0fb651be       movzx edx, byte [rcx - 0x42]
0x000191b1      c1e210         shl edx, 0x10
0x000191b4      09c2           or edx, eax
0x000191b6      0fb649bf       movzx ecx, byte [rcx - 0x41]
0x000191ba      c1e118         shl ecx, 0x18
0x000191bd      09d1           or ecx, edx
0x000191bf      781a           js 0x191db
0x000191c1      41c707010000.  mov dword [r15], 1
```

So that loads a double word from memory, by shifting each byte back into
position. But the branch only checks if the sign flag is set during the
last `or`, meaning that the byte at `rcx - 0x41` must be under 0x80 not to
trigger the sign flag. Considering this, the whole crypto part ends up
simply having the final value checked for it's sign...


### Oh well...

The bruteforce has solved it already, not the best keygen but eh;

```sh
$ grep -B1 ^valid out
ZNBZQ.SB323.FHXWP.4BZXZ.CNDHQ
valid license
```


## Conclusion

There were a few parts thatI wasn't looking forward to analyze due to how
tedious it was to understand.
Especially the `0x18ebc-0x18ed6` area and up until the crypto calls, which is
most likely a setup for the input parameters (e.g. salt) for the hashing and
cypto keys.

But overall it was really nice and enjoyable. Looking forward to more stuff
like this. :sake:
